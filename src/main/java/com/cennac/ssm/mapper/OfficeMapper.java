package com.cennac.ssm.mapper;

import com.cennac.ssm.model.Office;

public interface OfficeMapper {
    int deleteByPrimaryKey(String id);

    int insert(Office record);

    int insertSelective(Office record);

    Office selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Office record);

    int updateByPrimaryKey(Office record);
}